
# Practica compra
# Practica compra

def main():

    habitual = ("patatas", "leche", "pan")

    especificos = []
    
    while True:
        item = input("Elemento a comprar: ")
        if item == (""):
            break

        if item not in habitual:    
            especificos.append(item)

    elementos_en_lista = (list(habitual) + especificos)

    print("Lista de la compra: ")
    
    print(*habitual)
    print(*especificos)

    elementos_habituales = len(habitual)
    elementos_especificos = len(especificos)
    elementos_totales = len(elementos_en_lista)

    print(f"Elementos habituales: {elementos_habituales}")
    print(f"Elementos específicos: {elementos_especificos}")
    print(f"Elementos en lista: {elementos_totales}")

if __name__ == '__main__':
    main()